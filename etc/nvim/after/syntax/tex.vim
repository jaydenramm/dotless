syntax match texStatement '\\ce' nextgroup=texMyCommand
syntax match texStatement '\\si' nextgroup=texMyCommand
syntax region texMyCommand matchgroup=Delimiter start='{' end='}' contained contains=@NoSpell
