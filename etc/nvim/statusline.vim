hi Sl1 guifg=#3a4755 guibg=NONE gui=bold
hi Sl2 guifg=#ebeef1 guibg=NONE gui=bold
hi Sl3 guifg=#a5b3c1 guibg=NONE gui=NONE
hi Slrese            guibg=NONE

function! SetModifiedSymbol(modified)
	if a:modified == 1
		return '* '
	else
		return ''
	endif
endfunction

set statusline=%#Slrese#\ \ %#Sl1#
set statusline+=%=
set statusline+=%#Slrese#
set statusline+=%#Sl2#\ %.80t\ 
set statusline+=%#Sl3#%{SetModifiedSymbol(&modified)}
set statusline+=\%#Sl1#\[
set statusline+=\%#Sl3#\ %l,%c\ 
set statusline+=\%#Sl1#\]
set statusline+=\%#Sl1#\ 
