"vim-plugins
call plug#begin('~/etc/nvim/plugged')

Plug 'lervag/vimtex'
Plug 'ap/vim-css-color'
Plug 'SirVer/ultisnips'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/goyo.vim'
Plug 'dense-analysis/ale'
Plug 'nvim-lua/popup.nvim'
Plug 'tpope/vim-commentary'
Plug 'sheerun/vim-polyglot'
Plug 'itmammoth/doorboy.vim'
Plug 'plasticboy/vim-markdown'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }

call plug#end()
