"fix some filetypes
autocmd BufNewFile,BufRead *.md set filetype=markdown
autocmd BufNewFile,BufRead *.tex set filetype=tex
autocmd BufNewFile,BufRead *.cls set filetype=tex
autocmd BufNewFile,BufRead *.sty set filetype=tex

"remove some markdown bloat
autocmd FileType md,markdown ALEDisable
autocmd FileType md,markdown CocDisable

"vim-markdown
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_conceal_code_blocks = 0
let g:vim_markdown_math = 0
let g:vim_markdown_toml_frontmatter = 1
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_strikethrough = 1
let g:vim_markdown_autowrite = 1
let g:vim_markdown_edit_url_in = 'tab'
let g:vim_markdown_follow_anchor = 0
let g:vim_markdown_new_list_item_indent = 0
let g:vim_markdown_auto_insert_bullets = 0

"ale
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'tex': ['latexindent', 'textlint'],
\   'html': ['prettier'],
\   'javascript': ['prettier'],
\   'css': ['prettier'],
\   'cpp': ['clang-format', 'clangtidy'],
\   'c': ['clang-format', 'clangtidy'],
\}
let g:ale_linters = {
\   'cpp': ['cc', 'clangd', 'clangtidy'],
\   'c': ['cc', 'clangd', 'clangtidy'],
\}
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_insert_leave = 0
let g:ale_fix_on_save = 1
let g:ale_sign_error = '>'
let g:ale_sign_warning = '-'
let g:ale_sign_info= '^'

"vimtex
autocmd FileType tex,plaintex call vimtex#init()
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0
let g:tex_conceal= 'bs'
let g:tex_fast = "bMpr"
nnoremap <leader>ss <cmd>:VimtexTocOpen<CR>

"coc-nvim
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <C-n>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<C-n>" :
      \ coc#refresh()
let g:coc_status_error_sign = '^'
let g:coc_status_warning_sign= 'x'

"fzf.vim
let g:fzf_preview_window = ['right:50%:hidden', 'ctrl-/']
nnoremap <leader>ff <cmd>:Files<CR>
nnoremap <leader>gf <cmd>:GitFiles<CR>
nnoremap <leader>dd <cmd>:Buffers<CR>

"ultiSnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<S-tab>"

"go yo
function! s:goyo_enter()
  hi NonText guifg=#1a2026
endfunction

function! s:goyo_leave()
  hi NonText guifg=#1a2026
  colo vybe
endfunction

let g:goyo_linenr = 1
let g:goyo_width = 100
nnoremap <C-g> :Goyo<CR>

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()
