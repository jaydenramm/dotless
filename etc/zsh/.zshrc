#comments
set -k

#sources
source "$HOME/etc/zsh/.alii"
source "$HOME/etc/zsh/.high"

#dir name cd
setopt auto_cd
setopt autocd extendedglob nomatch notify

#completion
setopt NO_NOMATCH
setopt complete_in_word
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu select
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' matcher-list \
	'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
autoload -U compinit && compinit -C

#compinit
autoload -Uz compinit
compinit

#history
export HISTFILE="$HOME/etc/zsh/.histfile"
export HISTSIZE=42069
export SAVEHIST=42069
setopt hist_ignore_dups
setopt histignorespace
setopt share_history

#disable dumb stuff
unsetopt flowcontrol
unsetopt beep

#set binds
lu() {
	autoload -U "$1"; zle -N "$1"; bindkey "$2" "$1";
}

#keybinds
bindkey '^a'	beginning-of-line
bindkey '^e'	end-of-line
bindkey '^w'	backward-kill-word
bindkey '^f'	forward-char
bindkey '^b'  backward-char
bindkey '^r'  history-incremental-search-backward

#history
lu up-line-or-beginning-search   '^[[A'
lu down-line-or-beginning-search '^[[B'
lu up-line-or-beginning-search   '^p'
lu down-line-or-beginning-search '^n'
lu edit-command-line '^x'

#git status on ^b
kgs() { clear; git status -sb; zle redisplay; }
zle -N kgs; bindkey '^g' kgs

#ls on ^k
kls() { clear; ls -CFv --color=auto --group-directories-first;\
	zle redisplay; }
zle -N kls; bindkey '^k' kls

#^l does actual clear
klear() { clear; \
	zle redisplay; }
zle -N klear; bindkey '^l' klear

#^y for stuf
ktd() { nvim "$HOME/rey/stuf"; \
	zle redisplay; }
zle -N ktd; bindkey '^y' ktd

#functions
lc() { cat "$@" | wc -l }
li() { grep -icv '^[[:space:]]*#\|^$' "$@" }
bcpp() { g++ ./*.cpp -o ./program }
copy() {cat "$1" | xclip -sel clip -in }

#webdev
bs() {
	cd $HOME/web/public/ && \
		sass -t nested --watch css/design.scss:css/style.css & PIDIOS=$!
		browser-sync & PIDMIX=$!
		wait $PIDIOS
		wait $PIDMIX
		rm -rf .sass-cache/
		rm css/style.css.map
}

#fff cd on exit
f() {
	fff "$@"
	cd "$(cat "${XDG_CACHE_HOME:=${HOME}/.cache}/fff/.fff_d")"
}

#heh
command_not_found_handler() {
	echo -e "\e[31m'$0'\e[0m wasn't found"
	return 1
}

#prompt
export PROMPT="%B%F{0}[%f%F{255}%1~%f%F{0}]%F{3} ->%f%b "
export SUDO_PROMPT=$'pwd for\033[38;32'"${acc}m %u"$'\033[0m '

#fzf
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_OPTS='
	--color fg:#ebeef1,bg:#1a2026,hl:#86cf90,fg+:#bcc7d1,bg+:#222a32,hl+:#86cf90
	--color pointer:#cf9086,info:#3a4755,spinner:#3a4755,header:#3a4755,prompt:#86a1cf,marker:#cfb586
'
